import xlrd,os
from new2.conf import BASE_PATH

class ReadFile:
    # config_dict = None
    # config_path = f"{str(Path(__file__).parent.parent)}/config/config.yaml".replace('\\','/')
    #.replace('\\','/')
    #D:\Tools\new\tools/config/config.yaml
    #D:\Tools\new/config/config.yaml


    # @classmethod
    # def read_testcase(cls):
    #     """
    #     读取excel格式的测试用例,返回一个生成器对象
    #     :return 生成器
    #     """
    #     book = xlrd.open_workbook(cls.read_config("$.file_path.testcase"))
    #     # 读取第一个sheet页
    #     table = book.sheet_by_index(0)
    #     for norw in range(1, table.nrows):
    #         # 每行第4列 是否运行
    #         if table.cell_value(norw, 4) != "否":  # 每行第4列等于否将不读取内容
    #             value = table.row_values(norw)
    #             value.pop(4)
    #             yield value


    def read_testcase2(self,table_name) :
        """
        读取excel格式的测试用例,返回一个生成器对象
        :return case的列表
        """
        filename="case_data.xls"
        excelfir = BASE_PATH + os.sep + "Data" + os.sep + filename
        workbook = xlrd.open_workbook(excelfir, formatting_info=True)
        table = workbook.sheet_by_name(table_name)
        # data_list = []
        for norw in range(1, table.nrows):
            # 每行第4列 是否运行
            if table.cell_value(norw, 4) != "否":  # 每行第4列等于否将不读取内容
                value = table.row_values(norw)
                value.pop(4)
                # 配合将每一行转换成元组存储，迎合 pytest的参数化操作，如不需要可以注释掉 value = tuple(value)
                # value = tuple(value)
                # print(f'{value}')
                # data_list.append(value)
                yield value


if __name__ == '__main__':
    # ab=ReadFile().config_path
    # print(ab)
    rd=ReadFile().read_testcase2("华研")
    print(rd)
