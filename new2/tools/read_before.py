import re,allure,json
from string import Template

from jsonpath import jsonpath

from new2.tools.LOG import GetLog
log=GetLog.get_logger()

def exec_func(func):
    """执行函数相当于函数助手(exec可以执行Python代码)
    :params func 字符的形式调用函数
    : return 返回的将是个str类型的结果
    """
    # 得到一个局部的变量字典，来修正exec函数中的变量，在其他函数内部使用不到的问题
    loc = locals()
    exec(f"result = {func}")
    return str(loc['result'])


def extractor(obj, expr= '.') :
    """
    根据表达式提取字典中的value，表达式, . 提取字典所有内容， $.case 提取一级字典case， $.case.data 提取case字典下的data
    :param obj :json/dict类型数据
    :param expr: 表达式, . 提取字典所有内容， $.case 提取一级字典case， $.case.data 提取case字典下的data
    $.0.1 提取字典中的第一个列表中的第二个的值
    """
    try:
        result = jsonpath(obj, expr)[0]

    except Exception :
        log.error(f'{expr} - 提取不到内容，请检查表达式是否有误！')
        result = expr

        # raise


    return result


def rep_expr(content, data) :
    """从请求参数的字符串中，使用正则的方法找出合适的字符串内容并进行替换
    :param content: 原始的字符串内容
    :param data: 提取的参数变量池
    return content： 替换表达式后的字符串
    """
    #将content中带有$的值替换为data中的值，$age,$name,abc,${get()},${abc}"
    #Template(s).safe_substitute(b),结果显示为1,$name,abc,${get()},${abc}
    content = Template(content).safe_substitute(data)
    #正则进行替换每一个带$的值为输出后的结果值
    for func in re.findall('\\${(.*?)}', content):
        try:
            content = content.replace('${%s}' % func, exec_func(func))
            log.info("调用rep_expr方法替换后的数据为：{}".format(content))
        except Exception as e:
            log.error("调用rep_expr方法替换失败，表达式有误或变量池为空，具体原因为：{}".format(e))
            raise
    return content



def convert_json(dict_str):
    """将字符串转化为json格式
    :param dict_str: 长得像字典的字符串
    return json格式的内容
    """
    try:
        if 'None' in dict_str:
            dict_str = dict_str.replace('None', 'null')
        elif 'True' in dict_str:
            dict_str = dict_str.replace('True', 'true')
        elif 'False' in dict_str:
            dict_str = dict_str.replace('False', 'false')
        dict_str = json.loads(dict_str,strict=False)
    except Exception as e:
        if 'null' in dict_str:
            dict_str = dict_str.replace('null', 'None')
        elif 'true' in dict_str:
            dict_str = dict_str.replace('true', 'True')
        elif 'false' in dict_str:
            dict_str = dict_str.replace('false', 'False')
        dict_str = eval(dict_str)
        log.error(e)

    return dict_str


def allure_title(title):
    """allure中显示的用例标题"""
    allure.dynamic.title(title)


def allure_step(step, var) :
    """
    :param step: 步骤及附件名称
    :param var: 附件内容
    """
    with allure.step(step):
        allure.attach(
            json.dumps(
                var,
                ensure_ascii=False,
                indent=4),
            step,
            allure.attachment_type.JSON)
#
#
# def allure_step_no(step):
#     """
#     无附件的操作步骤
#     :param step: 步骤名称
#     :return:
#     """
#     with allure.step(step):
#         pass

