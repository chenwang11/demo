#-*- coding: utf-8 -*-
#@File      :Get_yjyx_excel_data.py
#@Time      :2021/3/17 10:55
#@Author    :chenwang
#@Emall     :275508210@qq.com
#@Software  :PyCharm
import xlrd,json,os
# from xlutils.copy import copy
from new2.conf import  BASE_PATH

def get_excel_data(sheet_name,casename):
    resList = []
    filename="case_data.xls"
    excelfir=BASE_PATH +os.sep+"Data"+os.sep+filename
    workbook=xlrd.open_workbook(excelfir,formatting_info=True)
    work_sheet=workbook.sheet_by_name(sheet_name)


    col=work_sheet.col_values(0)#获取第一列
    # print(col)
    idx = 0#开始的下标
    for one in col:
        if casename in one:
            case_num = work_sheet.cell_value(idx, 0)#用例编号
            print(case_num)
            case_title = work_sheet.cell_value(idx, 1)#用例标题

            headers = work_sheet.cell_value(idx, 2)#请求头

            url = work_sheet.cell_value(idx, 3)#URL

            method = work_sheet.cell_value(idx, 5)#请求方式

            parametric_key = work_sheet.cell_value(idx, 6)#入参关键字

            files = work_sheet.cell_value(idx, 7)#上传文件

            data = work_sheet.cell_value(idx, 8)#请求参数

            extra = work_sheet.cell_value(idx, 9)#提取参数

            sql = work_sheet.cell_value(idx, 10)#后置sql

            expect = work_sheet.cell_value(idx, 11)#预期结果

            # indata_json=indata.replace('\n','').replace('\r\n','')
            # except_result = work_sheet.cell_value(idx, 11)#预期结果
            # resList.append((case_name, json.loads(indata, strict=False)))
            if data:
                data=json.loads(data, strict=False)
            # if expect:
            #     expect = json.loads(expect, strict=False)


            resList.append((case_num,case_title,headers,url,method,parametric_key,
                            files,data,extra,sql,expect))
        idx+=1
    return resList
def convert_json(dict_str):
    """将字符串转化为json格式
    :param dict_str: 长得像字典的字符串
    return json格式的内容
    """
    if 'None' in dict_str:
        dict_str = dict_str.replace('None', 'null')
    elif 'True' in dict_str:
        dict_str = dict_str.replace('True', 'true')
    elif 'False' in dict_str:
        dict_str = dict_str.replace('False', 'false')
    dict_str = json.loads(dict_str)
    if 'null' in dict_str:
        dict_str = dict_str.replace('null', 'None')
    elif 'true' in dict_str:
        dict_str = dict_str.replace('true', 'True')
    elif 'false' in dict_str:
        dict_str = dict_str.replace('false', 'False')
    dict_str = eval(dict_str)
    return dict_str
from xlutils.copy import copy
def set_excel_data(num):
    #1-excel 表路径
    filename = "合同用例1.0.xls"
    excelfir = BASE_PATH + os.sep + "Data" + os.sep + filename
    #2-打开Excel对象--formatting_info=True 保持样式
    workbook = xlrd.open_workbook(excelfir, formatting_info=True)
    workbooknew=copy(workbook)#复制一个新Excel文件对象
    worksheetnew=workbooknew.get_sheet(num)#取复制出来新Excel对象的第一个字表
    return workbooknew,worksheetnew  #复制出来的Excel对象，复制出来新Excel对象的第一个字表
if __name__ == '__main__':
    ret=get_excel_data("用例","c1")
    print(ret)



