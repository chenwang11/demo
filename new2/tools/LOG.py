#导包
import logging.handlers
import os,datetime
from new2.conf import BASE_PATH
class GetLog:
    #新建一个日志管理
    __logger=None
    #新建获取日志器的方法,保证至始至终获取的是同一个对象，以及保证通过类名.可以快速调取到日志器的方法，所以使用类方法
    @classmethod
    def get_logger(cls):
        #判断日志器是否为空     确保调用多次时，使用的是同一个日志器（判断是否为空设置）
        if cls.__logger is None:#说明第一次调用
            #获取日志器
            cls.__logger=logging.getLogger()
            #修改默认级别
            cls.__logger.setLevel(logging.INFO)

            log_path=BASE_PATH + os.sep+"Log"+os.sep+"hmtt.log"

            #获取处理器
            th = logging.handlers.TimedRotatingFileHandler(filename=log_path,
                                                           when="midnight",
                                                           interval=1,
                                                           backupCount=3,
                                                           encoding="utf_8")#时间24小时，间隔1，备份的数量3，编码utf-8
            #获取格式器
            fmt="%(asctime)s %(levelname)s [%(filename)s(%(funcName)s:%(lineno)id)] - %(message)s"
            # fmt = "%(asctime)s %(levelname)s [%(name)s] [%(filename)s (%(funcName)s:%(lineno)d] - %(message)s"
            fm=logging.Formatter(fmt)
            # 输出控制台
            fh_stream = logging.StreamHandler()
            fh_stream.setFormatter(fm)

            #将格式添加到处理器中
            th.setFormatter(fm)

            #将处理器添加到日志器中
            cls.__logger.addHandler(th)
            cls.__logger.addHandler(fh_stream)
        #返回日志器
        return cls.__logger

if __name__ == '__main__':
    log=GetLog().get_logger()
    log.info("测试信息级别日志")
#     log.error("测试错误级别日志")
# 2022-10-11 16:20:47,993 INFO [LOG.py(<module>:45d)] - 测试信息级别日志
# 2022-10-11 16:24:52,150 INFO [root] [LOG.py (<module>:46] - 测试信息级别日志
