# from new.tools.read_before import extractor, convert_json, rep_expr, allure_step, allure_step_no
from new2.tools.read_before import *
from new2.tools.db import Mysql
from new2.tools.read_file import ReadFile
from new2.tools.LOG import GetLog
log=GetLog.get_logger()
from new2.conf import request_headers,host
class DataProcess:
    # 存放提取参数的池子
    extra_pool = {}
    header=request_headers

    @classmethod
    def handle_path(cls, path_str):
        """路径参数处理
        :param path_str: 带提取表达式的字符串 /${id}/state/${create_time}
        :param env: 环境名称， 对应的是环境基准地址
        上述内容表示，从extra_pool字典里取到key为id 对应的值，假设是500，后面${create_time} 类似， 假设其值为 1605711095 最终提取结果
        return  /511/state/1605711095
        """
        if '$' in path_str:
            url = host + rep_expr(path_str, cls.extra_pool)
        else:
            url=host+path_str
        allure_step("url", url)
        return url

    @classmethod
    def handle_header(cls, header_str):
        """处理header， 将用例中的表达式处理后 追加到基础header中
        :header_str: 用例栏中的header
        return header:
        """
        # if header_str == '':
        header_str = '{}'
        #将两个参数值合并为一个
        cls.header.update(cls.handle_data(header_str))
        allure_step('请求头', cls.header)
        return cls.header

    @classmethod
    def handler_files(cls, file_obj):
        """file对象处理方法
        :param file_obj: 上传文件使用，格式：接口中文件参数的名称:"文件路径地址"/["文件地址1", "文件地址2"]
        实例- 单个文件: &file&D:
        """
        # if file_obj != '':
        for k, v in convert_json(file_obj).items():
            # allure_step('上传文件', file_obj)
            # 多文件上传
            if isinstance(v, list):
                files = []
                for path in v:
                    files.append((k, (open(path, 'rb'))))
                    return files
            else:
                # 单文件上传
                files = {k: open(v, 'rb')}
                return files



    @classmethod
    def handle_data(cls, variable):
        """请求数据处理
        :param variable: 请求数据，传入的是可转换字典/json的字符串,其中可以包含变量表达式
        return 处理之后的json/dict类型的字典数据
        """
        # if variable :
        #如果含有$就需要从变量池中取出值替换
        if '$' in variable :
            #将variable替换为变量池中对应的value值
            variable = rep_expr(variable, cls.extra_pool)
            log.info("变量池数据：{}".format(cls.extra_pool))


        variable = convert_json(variable)
        return variable

    @classmethod
    def handle_sql(cls, sql, db=Mysql):
        """
        处理sql，如果sql执行的结果不会空，执行sql的结果和参数池合并
        :param sql: 支持单条或者多条sql，其中多条sql使用 ; 进行分割
            多条sql,在用例中填写方式如下select * from user; select * from goods 每条sql语句之间需要使用 ; 来分割
            单条sql,select * from user 或者 select * from user;
        :param db: 数据库连接对象
        :return:
        """
        sql = rep_expr(sql, cls.extra_pool)

        for sql in sql.split(";"):
            sql = sql.strip()
            if sql == '':
                continue
            # 查后置sql
            result = Mysql().fetchone(sql)
            allure_step(f'执行sql: {sql}', result)
            if result is not None:
                # 将查询结果添加到响应字典里面，作用在，接口响应的内容某个字段 直接和数据库某个字段比对，在预期结果中
                # 使用同样的语法提取即可
                cls.extra_pool.update(result)

    @classmethod
    def handle_extra(cls, extra_str: str, response: dict):
        """
        处理提取参数栏
        :param extra_str: excel中 提取参数栏内容，需要是 {"参数名": "jsonpath提取式"} 可以有多个
        :param response: 当前用例的响应结果字典
        """
        # if extra_str != '':

        #调用convert_json转字典格式
        extra_dict = convert_json(extra_str)
        #进行循环，将extra的key和通过value表达式取出的响应结果值保存到变量池extra_pool
        for k, v in extra_dict.items():
            # try:
            cls.extra_pool[k] = extractor(response, v)
            log.info(f'加入依赖字典,key: {k}, 对应value: {v}')

    @classmethod
    def assert_result(cls, response: dict, expect_str: str):
        """ 预期结果实际结果断言方法
        :param response: 实际响应结果
        :param expect_str: 预期响应内容，从excel中读取
        return None
        """
        # 后置sql变量转换
        log.info("当前可用参数池为{}".format(cls.extra_pool) )
        allure_step("当前可用参数池", cls.extra_pool)
        if expect_str:
            #特殊情况处理，如果响应结果返回一个{}，如果没有这种情况，else可全部去掉
            if expect_str!='{}':
                expect_str = rep_expr(expect_str, cls.extra_pool)
                expect_dict = convert_json(expect_str)
                index = 0
                for k, v in expect_dict.items():
                    # 获取需要断言的实际结果部分
                    actual = extractor(response, k)
                    index += 1
                    log.info(f'第{index}个断言,实际结果:{actual} | 预期结果:{v} \n断言结果 {actual == v}')
                    allure_step(f'第{index}个断言', f'实际结果:{actual} = 预期结果:{v}')
                    try:
                        #预期结果==实际结果
                        assert actual == v
                    except AssertionError:
                        raise AssertionError(
                            f'第{index}个断言失败 -|- 实际结果:{actual} || 预期结果: {v}')
            else:
                expect_dict = convert_json(expect_str)
                allure_step("断言结果", f'(实际结果：{expect_dict} = 预期结果：{response}')
                log.info(f'实际结果:{response} | 预期结果:{expect_str}\n断言结果 {expect_dict == response}')
                try:
                    # 预期结果==实际结果
                    assert  expect_dict==response
                except AssertionError:
                    raise AssertionError(f' 实际结果:{response} || 预期结果: {expect_str}')

