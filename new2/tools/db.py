import pymysql
from new2.tools.LOG import GetLog
from new2.conf import db
log=GetLog.get_logger()
#1、创建封装类
class Mysql:
    def __init__(self):
        self.conn = pymysql.connect(
            host=db['host'],
            user=db['user'],
            password=db['password'],
            database=db['db_name'],
            charset=db['charset'],
            port=db['port']
        )
        self.cursor = self.conn.cursor(cursor=pymysql.cursors.DictCursor)

#3、创建查询、执行方法
    def fetchone(self,sql):
        """
        单个查询
        :param sql:
        :return:
        """
        log.info("正在调用查询单个sql：fetchone方法，sql语句为：{}".format(sql))
        self.cursor.execute(sql)
        ret_db=self.cursor.fetchone()
        log.info("查询sql语句返回结果为：{}".format(ret_db))
        return ret_db

    def fetchall(self,sql):
        """
        多个查询
        :param sql:
        :return:
        """
        log.info("正在调用查询多个sql：fetchall方法，sql语句为：{}".format(sql))
        self.cursor.execute(sql)
        ret_db = self.cursor.fetchall()
        log.info("查询sql语句返回结果为：{}".format(ret_db))
        return ret_db

    def exec(self,sql):
        """
        执行非查询语句，如增删改
        :return:
        """
        log.info("正在调用exec方法，sql语句为：{}".format(sql))
        try:
            if self.conn and self.cursor:
                self.cursor.execute(sql)
                self.conn.commit()
        except Exception as ex:
            self.conn.rollback()
            log.error("Mysql执行失败,原因为：{}".format(ex))
            return False
        return True

#4、关闭对象
    def __del__(self):
        #关闭光标对象
        if self.cursor is not None:
            self.cursor.close()
        #关闭连接对象
        if self.conn is not None:
            self.cursor.close()

if __name__ == "__main__":
    # mysql = Mysql("211.103.136.242",
    #               "test",
    #               "test123456","meiduo",
    #               charset="utf8",
    #               port=7090)
    mysql = Mysql()
    res = mysql.fetchall("select username,password from tb_users")
    # res = mysql.exec("update tb_users set first_name='python' where username = 'python'")
    # print(res)