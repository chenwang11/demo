#-*- coding: utf-8 -*-
#@File      :Get_yjyx_excel_data.py
#@Time      :2021/3/17 10:55
#@Author    :chenwang
#@Emall     :275508210@qq.com
#@Software  :PyCharm
import xlrd,json,os
# from xlutils.copy import copy
from hyht.conf import  BASE_PATH
def get_excel_data(sheet_name,casename):
    resList = []
    filename="合同用例2.0.xls"
    excelfir=BASE_PATH +os.sep+"Data"+os.sep+filename
    workbook=xlrd.open_workbook(excelfir,formatting_info=True)
    work_sheet=workbook.sheet_by_name(sheet_name)


    col=work_sheet.col_values(0)#获取第一列
    # print(col)
    idx = 0#开始的下标
    for one in col:
        if casename in one:
            case_name = work_sheet.cell_value(idx, 0)#获取每次循环行的第一列名称
            print(case_name)
            # indata=work_sheet.cell_value(idx,9)#请求参数api
            indata = work_sheet.cell_value(idx, 4)  # 请求参数ui
            # indata_json=indata.replace('\n','').replace('\r\n','')
            except_result = work_sheet.cell_value(idx, 7)#预期结果
            # resList.append((case_name, json.loads(indata, strict=False)))


            resList.append((case_name,json.loads(indata,strict=False),json.loads(except_result,strict=False)))
        idx+=1
    return resList


if __name__ == '__main__':
    ret=get_excel_data("功能用例","logint")
    print(ret)