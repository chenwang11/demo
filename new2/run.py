import sys,os,pytest,allure

# sys.path.append("d:/Tools")#绝对路径：添加上一级目录到环境变量，当前路径为‘d:/Tools/hyht’
# sys.path.append(os.getcwd()+os.sep+'..')#相对路径：添加上一级目录到环境变量



if __name__ == '__main__':
    for one in os.listdir('./report/tmp'):  # 列出对应文件夹的数据
        if 'json' in one:
            os.remove(f'./report/tmp/{one}')
    # pytest.main(['test_myShop.py', '-s', '--alluredir', './report/tmp'])
    # print(__file__)
    # print(os.getcwd())#当前路径
    # pytest.main(['./testcase/api/test_account.py', '-s', '-k','test2','-v', '--alluredir', './report/tmp'])
    pytest.main(['./testcase/api/test_org.py', '-s','-v', '--alluredir', './report/tmp'])
    # os.system('allure generate ./report/tmp -o ./report/report --clean')#这个能生成一个report文件数据，有HTML信息