import time

import pytest
from hyht.page.homepage import HomePage,SchedulePage
from hyht.page.loginpage import LoginPage
from hyht.tools.mydriver import Get_Driver
class TestSchedule():
    def setup_class(self):
        LoginPage().login('275508210@qq.com',"443306042")
        self.home=HomePage()
        self.schedule=SchedulePage()

    def teardown_class(self):
        #driver在base中有，子类继承父类
        time.sleep(10)
        Get_Driver.close_browser()
    def test_new_schedule(self):
        self.home.to_schedule()
        # self.schedule.new_schedule(theme,star_time,end_time,select_user,address,description)
        self.schedule.new_schedule("劳动节快乐", ["小明"], "长沙市", "这是通知")
        # self.schedule.new_schedule("劳动节快乐", "2021-05-02 18:00", "2021-05-02 19:00", "王小明", "长沙市", "这是一个刚发布的通知")
        self.home.logout()
        LoginPage().login('wangxiaoming@qq.com',"443306042")

if __name__ == '__main__':
    pytest.main(["test_add_schedule.py", "-s"])