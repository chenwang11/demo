import time,random
from random import randint
import pytest,allure
from hyht.page.homepage import HomePage
from hyht.page.peoplepage import PeoplePage
from hyht.tools.Get_excel_data2 import get_excel_data
from hyht.tools.get_log import GetLog
log=GetLog.get_logger()
@allure.feature('人员模块')
class Test_People:
    @allure.title('添加人员异常弹框测试')
    @pytest.mark.parametrize("casename,indata,except_result",get_excel_data("功能用例","people_addf"))
    def test_add_people1(self,casename,indata,except_result):
        name=indata['name']
        mobile=indata['mobile']
        text=indata['text']
        print("测试用例编号======",casename)
        time.sleep(5)#由于有异常弹框遮挡元素，需要等待5秒，下一个用例执行才能成功f
        HomePage().to_peoplepage()
        ret=PeoplePage().add_peoplef(name,mobile,text)
        try:
            assert ret==except_result['msg']
        except Exception as e:
            print("断言出错，原因：",e)
            log.error("断言出错，原因：{}".format(e))
            # log.error("{}-元素不可见-{}".format(doc, e))
            HomePage().save_screenshot(doc="断言失败截图")#这边需要一个类提供driver
            raise
    @pytest.mark.add_people
    @allure.title("新增人员成功用例")
    def test_people_addt020(self):
        at=random.randint(1000,9999)
        name=f"李四{at}"
        mobile=f'1383838{at}'
        filename='123.jpg'
        text='客户'

        HomePage().to_peoplepage()
        ret=PeoplePage().add_people2(name,mobile,filename,text)
        try:
            assert ret==name
        except Exception as e:
            print("断言出错，原因：",e)
            log.error("断言出错，原因：{}".format(e))
            # log.error("{}-元素不可见-{}".format(doc, e))
            HomePage().save_screenshot(doc="断言失败截图")#这边需要一个类提供driver
            raise


    def test_find_people(self):
        ret=HomePage().to_peoplepage().find_people("王五")
        try:
            assert ret == "王五"
        except Exception as e:
            print("断言出错，原因：", e)
            log.error("断言出错，原因：{}".format(e))
            # log.error("{}-元素不可见-{}".format(doc, e))
            HomePage().save_screenshot(doc="断言失败截图")  # 这边需要一个类提供driver
            raise

    def test_modify(self):
        at = random.randint(1000, 9999)
        name = f"李四{at}"
        mobile = f'1383838{at}'
        text = "供应商"
        HomePage().to_peoplepage()
        PeoplePage().modify_people2(name,mobile,text)
        ret =PeoplePage().find_people(name)
        try:
            assert ret==name
        except Exception as e:
            print("断言出错，原因：",e)
            log.error("断言出错，原因：{}".format(e))
            # log.error("{}-元素不可见-{}".format(oc, e))
            HomePage().save_screenshot(doc="断言失败截图")#这边需要一个类提供driver
            raise
    @pytest.fixture(scope="class")
    def before_add_people(self):
        at = random.randint(1000, 9999)
        name = f"厉害{at}"
        mobile=f'1586485{at}'
        HomePage().to_peoplepage().add_people2(name,mobile,"123.jpg","客户")
        yield name

    def test_people_stop030(self,before_add_people):
        ret1=HomePage().to_peoplepage().find_people(before_add_people)
        assert ret1 == before_add_people
        ret=PeoplePage().stop_people(before_add_people)
        try:

            assert ret[0]=='已成功停用该用户。'
            assert ret[1]=="没有可显示的项目。"
        except Exception as e:
            print("断言出错，原因：",e)
            log.error("断言出错，原因：{}".format(e))
            # log.error("{}-元素不可见-{}".format(oc, e))
            HomePage().save_screenshot(doc="断言失败截图")#这边需要一个类提供driver
            raise
    def test_find_people_info(self):
        HomePage().to_peoplepage().find_people2("王五")
if __name__ == '__main__':
    pytest.main(["test_people.py", "-s","-k","test_find_people_info"])