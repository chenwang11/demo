import pytest,allure,time
from hyht.page.loginpage import LoginPage
from hyht.tools.mydriver import Get_Driver
from hyht.tools.get_log import GetLog
log=GetLog().get_logger()
@allure.title("登录")
@pytest.fixture(scope='session',autouse=True)
def init_login():
    log.info('正在初始化登录')
    LoginPage().login('18374977980','443306042')

    yield
    time.sleep(2)
    log.info('关闭浏览器，清除driver')
    Get_Driver.close_browser()