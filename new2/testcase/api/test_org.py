from pip._vendor.rich import json

from new2.lib.api.client import Client
from new2.tools.data_process import DataProcess
from new2.tools.read_file import ReadFile
import pytest

@pytest.mark.parametrize('case',ReadFile().read_testcase2("华研"))
def test2(case, init_login):

    case_num, case_title, header, path, method, parametric_key, file_obj, data, extra, sql, expect = case
    print(case_num)
    header = {"spaceid": init_login[1]}
    if data!='':
        #从Excel中读取出来的数据是str类型，需要转json再替换掉值后，再次转化为str类型给后续转json
        data=json.loads(data)
        if (data)['space'] !='':
            (data)['space']=init_login[1]
            data=json.dumps(data)

    response = Client().base(path, method, parametric_key,header=header,data=data,
                              file=file_obj, cookies=init_login[0],extra=extra)
    # 断言操作
    DataProcess.assert_result(response, expect)


if __name__ == '__main__':
    pytest.main(['test_org.py', '-s','-k','test2','-v', '--alluredir', './report/tmp'])
