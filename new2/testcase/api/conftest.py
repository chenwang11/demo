from new2.lib.api.client import Client
import pytest,allure,requests
from new2.conf import host,user1
from new2.tools.LOG import GetLog
log=GetLog().get_logger()
@allure.title('初始化登录,获取cookies')
@pytest.fixture(scope="session")
def init_login():
    '''

    :return: cookies:返回cookies;space_id:返回的space_id
    '''
    log.info("正在登录获取cookies")
    url=f'{host}/accounts/password/authenticate'
    data={"user":{"email":user1['user']},"password":user1['password'],"code":"","locale":"zh-cn"}
    ret = requests.post(url,json=data,verify=False,proxies={'http': 'http://127.0.0.1:8888', 'https': 'http://127.0.0.1:8888'})
    print(ret)
    cookies=ret.cookies
    space_id=cookies['X-Space-Id']
    log.info("获取的cookie为{}".format(cookies))
    return cookies,space_id
