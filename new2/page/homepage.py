from selenium.webdriver.common.by import By

from hyht import page
from hyht.page.pagelocate.homepagelocate import HomeLocate as hl
from hyht.page.pagelocate.schedulelocate import ScheduleLocate as sl
from hyht.lib.web_base import WebBase
# from hyht.page.loginpage import LoginPage

import time
from hyht.tools.get_log import GetLog

log=GetLog().get_logger()

class HomePage(WebBase):

    #进入日程界面
    def to_schedule(self):
        from hyht.page.peoplepage import PeoplePage
        self.base_click(hl.page_schedule,doc="进入日程界面")
        return SchedulePage()
    #退出登陆
    def logout(self):
        time.sleep(1)
        self.base_click(hl.page_out_img,doc="退出登录")
        # time.sleep(1)
        self.base_click(hl.page_drop_out)
    #进入人员界面
    def to_peoplepage(self):
        time.sleep(1)

        self.base_click(hl.page_people,doc="进入人员界面")
        from hyht.page.peoplepage import PeoplePage
        return PeoplePage()
    def info_alert(self):
        self.get_alert_txt()



class SchedulePage(HomePage):
    def click_add_schedule(self):
        self.base_click(sl.page_new_schedule,doc="点击新增日程")

        #输入主题
    def input_theme(self,theme):
        self.base_input(sl.page_input_theme,text=theme,doc="输入主题")
    def input_time(self,star_time,end_time):
        # 输入开始时间
        self.base_input(sl.star_time,text=star_time,doc="输入开始时间")
        time.sleep(1)
        # 输入结束时间
        self.base_input(sl.end_time,text=end_time,doc="输入结束时间")
    #点击指定的用户
    def user_click(self,click_user_name):
        loc=By.XPATH,"//font[text()='{}']".format(click_user_name)
        self.base_click(loc,doc="点击指定的用户")
    # 指派用户
    def select_ures(self,click_user_name):
        log.info("正在调用指派用户方法，用户名为{}".format(click_user_name))

        #判断click_user_name是否为list格式，是执行以下代码，不是就报错
        if isinstance(click_user_name,list):
            time.sleep(1)
            self.base_click(sl.select_user)
            time.sleep(2)
            #取消默认勾选的用户
            eles = self.wait_ele_presence(sl.select_users)
            for el in eles:
                el.click()
            time.sleep(1)
            for one in click_user_name:
                print(one)
                #点击指定的用户名称，base里面封装的
                self.user_click(one)
            time.sleep(1)
            self.base_click(sl.schedule_click_btn)
        else:
           raise print('指派用户格式不正确，请重新输入list格式')


    # 相关项
    # 勾选全天事件
    def is_all_day(self):
        self.base_click(sl.is_all_day)
    # 输入地址
    def input_address(self,address):
        log.info("输入地址为：{}".format(address))
        self.base_input(sl.address,address)
    # 输入描述内容
    def input_description(self,description):
        log.info("输入描述内容为：{}".format(description))
        self.base_input(sl.description,description)
    def get_erro_text(self):
        log.info("正在获取错误信息文本")
        return self.get_text(sl.get_erro_info)
    def save_schedule_btn(self):
        log.info("正在调用保存日程按钮方法")
        self.base_click(sl.save_schedule_btn)

    def new_schedule(self,theme,click_user_name,address,description):
        log.info("正在调用new_schedule方法，theme为{}，click_user_name为{}，address为{}，description为{}".format(theme,click_user_name,address,description))
        self.click_add_schedule()
        time.sleep(2)
        self.input_theme(theme)
        time.sleep(1)
        # self.input_time(star_time,end_time)
        self.select_ures(click_user_name)
        time.sleep(1)
        self.is_all_day()
        self.input_address(address)
        self.input_description(description)
        self.save_schedule_btn()

# class PeoplePage():
#     pass
    #1.设置js脚本控制滚动条
    # js="window.scrollTo(0,1000)"#(0:左边距，1000：上边距，单位像素)
    # #2.执行js脚本的方法
    # driver.execute_script(js)
if __name__ == '__main__':
    SchedulePage()