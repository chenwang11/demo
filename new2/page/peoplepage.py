from selenium.webdriver.common.by import By
from hyht.page.homepage import HomePage
from hyht import page
from hyht.page.pagelocate.peoplelocate import PeopleLocate as pl
from hyht.lib.base import Base
from hyht.page.loginpage import LoginPage
import time
from hyht.tools.get_log import GetLog

log=GetLog().get_logger()

class PeoplePage(HomePage):
    #新建人员，所有选项都填
    def add_people1(self,name,username,email,mobile,orgainzation,people_position
                   ,manager,contact_person,
                   work_phone,sort_no,text):

        log.info('正在调用新增人员方法')
        #点击新建
        self.base_click(pl.people_new,doc="点击新建")
        time.sleep(1)
        #输入姓名
        self.base_input(pl.people_name,text=name,doc="输入姓名")
        # 输入用户名
        self.base_input(pl.people_username,text=username,doc="输入用户名")
        # 输入邮件
        self.base_input(pl.people_email,text=email,doc="输入邮件")
        # 输入手机
        self.base_input(pl.people_mobile,text=mobile,doc="输入手机")
        # 输入所属部门
        self.base_input(pl.people_organizations,text=orgainzation,doc="输入所属部门")
        #输入职务
        self.base_input(pl.people_position,text=people_position,doc="输入职务")
        # 输入上级主管
        self.base_input(pl.people_manager,text=manager,doc="输入上级主管")
        # 输入联系人
        self.base_input(pl.people_contact_person,text=contact_person,doc="输入联系人")
        # 输入工作电话
        self.base_input(pl.people_work_phone,text=work_phone,doc="输入工作电话")
        # 输入排序号
        self.base_input(pl.people_sort_no,text=sort_no,doc="输入排序号")
        # 搜索简档
        self.find_profile(text)
        # 保存并新建
        self.base_click(pl.people_save_new,doc="保存并新建")

    def find_profile(self,text,doc='搜索简档'):
        '''
        :param text: 搜索简档,可以输入管理员，用户，供应商，客户
        :return:
        '''
        log.info("{}-输入文本为{}".format(doc,text))
        self.base_click(pl.people_jiandang,doc="点击简档")
        time.sleep(1)
        loc=By.XPATH,'//span[text()="{}"]'.format(text)
        # loc=By.XPATH,'//span[contains(text(),"{}")]'.format(text)
        self.base_click(loc,doc="点击所选简档名")
    def orgainzation(self,doc='所属部门'):
        log.info("选择{}".format(doc))
        self.base_click(pl.people_organizations,doc="点击所属部门")
        time.sleep(1)
        self.base_click(pl.people_select_organiz,doc="选择部门")
        time.sleep(1)
        self.base_click(pl.people_button1,doc="点击确定")
    #搜索联系人
    # def contact_person(self):
    #     pass

    def add_people2(self,name,mobile,
                  filename,text ):
        '''

        :param name: 姓名
        :param mobile: 手机号
        :param orgainzation: 所属部门
        :param text: 搜索简档
        :return:
        '''
        log.info('正在调用新增人员方法,只填必选')
        self.base_click(pl.people_new,"点击新建")
        time.sleep(1)
        self.base_input(pl.people_name, text=name,doc="输入姓名")
        self.base_input(pl.people_mobile, text=mobile,doc="输入手机")
        # 输入所属部门
        self.orgainzation()
        time.sleep(1)
        #搜索简档
        self.find_profile(text)
        self.upload_file_uninput(pl.people_select_files,filename)
        time.sleep(1)
        self.base_click(pl.people_save_new,doc="保存并新建")
        self.base_click(pl.people_close_page,doc="关闭新建页面")
        # 这边有个坑，获取的文本后面都带空格
        result = self.get_text_first(pl.people_infos, doc="获取name文本").replace(' ','')
        return result
    #新建人员，异常弹框测试
    def add_peoplef(self, name, mobile,
                    text):
        '''
        :param name: 姓名
        :param mobile: 手机号
        :param orgainzation: 所属部门
        :param text: 搜索简档
        :return:
        '''
        log.info('正在调用新增人员方法,只填必选，测试异常弹框')
        self.base_click(pl.people_new, "点击新建")
        time.sleep(1)
        self.base_input(pl.people_name, text=name, doc="输入姓名")
        self.base_input(pl.people_mobile, text=mobile, doc="输入手机")
        # 输入所属部门
        self.orgainzation()
        time.sleep(1)
        # 搜索简档
        self.find_profile(text)
        time.sleep(1)
        self.base_click(pl.people_save_new, doc="保存并新建")
        time.sleep(1)
        #获取错误信息
        result=self.get_text(pl.people_error_info,doc="获取异常报错文本")
        self.base_click(pl.people_close_page, doc="点击关闭新建页面")
        time.sleep(2)
        # self.page_refresh()
        return result
    #查找人员
    def find_people(self,name):
        self.base_input(pl.people_find,text=name,doc="输入搜索名称")
        self.enter()
        time.sleep(1)
        if name!=None:
            ret=self.get_text(pl.people_find_infos,doc="有数据获取文本")
        else:
            ret=self.get_text(pl.people_find_info,doc="无数据获取文本")

        print("diyige>>",ret)
        return ret

    # 查找指定人员详情
    def find_people2(self, name,doc="查找人员详情"):
        log.info("正在调用{}方法".format(doc))
        self.base_input(pl.people_find, text=name, doc="输入搜索名称")
        self.enter()
        time.sleep(1)
        self.base_click(pl.people_find_infos,doc="点击指定人员")
        time.sleep(1)
        ret=self.get_text_first(pl.people_infos,mode=2)
        name=ret[2].strip()
        print(name)
        return name



    def modify_people(self,name,mobile,filename,text,doc="修改人员带已有图片的"):
        log.info("正在调用{}方法".format(doc))
        self.base_click(pl.people_tubiaos,doc="点击小三角图标")
        self.base_click(pl.people_modify,doc="点击修改")
        time.sleep(1)
        self.base_input(pl.people_name, text=name, doc="输入姓名")
        self.base_input(pl.people_mobile, text=mobile, doc="输入手机")
        # 输入所属部门
        self.orgainzation()
        time.sleep(1)
        self.base_click(pl.people_del_jiandan,"x掉简档图标")
        # 搜索简档
        self.find_profile(text)
        self.base_click(pl.people_added_image,doc="移除已添加图片")
        self.upload_file_uninput(pl.people_select_files, filename)
        time.sleep(1)
        self.base_click(pl.people_save_new, doc="保存并新建")
        self.base_click(pl.people_close_page, doc="关闭新建页面")
        result = self.get_text_first(pl.people_infos, doc="获取name文本")
        return result

    def modify_people2(self,name,mobile,text,doc="修改人员"):
        log.info("正在调用{}方法name为{}mobile为{}text为{}".format(doc,name,mobile,text))
        self.base_click(pl.people_tubiaos,doc="点击小三角图标")
        self.base_click(pl.people_modify,doc="点击修改")
        time.sleep(1)
        self.base_input(pl.people_name, text=name, doc="输入姓名")
        self.base_input(pl.people_mobile, text=mobile, doc="输入手机")

        self.base_click(pl.people_del_jiandan,"x掉简档图标")
        # 搜索简档
        self.find_profile(text)
        # self.base_click(pl.people_added_image,doc="移除已添加图片")
        # self.upload_file_uninput(pl.people_select_files, filename)
        time.sleep(1)
        self.base_click(pl.people_save_new, doc="保存并新建")
        time.sleep(1)

        self.base_click(pl.people_close_page, doc="点击关闭新建页面")
        # self.base_click(pl.people_close_page, doc="点击关闭新建页面")
        result = self.get_text_first(pl.people_infos, doc="获取name文本")
        return result
    def stop_people(self,name,doc='停用人员'):
        log.info("正在调用{}方法".format(doc))
        self.base_click(pl.people_find_infos,doc="点击第一个人员")
        time.sleep(1)
        self.base_click(pl.people_stop,doc="点击停用")
        result=self.get_text(pl.people_stop_info,doc='获取提示文本')
        time.sleep(4)
        HomePage().to_peoplepage()
        self.base_input(pl.people_find, text=name, doc="输入搜索名称")
        self.enter()
        result2=self.get_text(pl.people_find_info)
        return result,result2