from hyht.conf import host
from hyht.page.pagelocate.loginlocate import LoginLocate as ll
from hyht.lib.web_base import WebBase

from hyht.tools.get_log import GetLog

log=GetLog().get_logger()

class LoginPage(WebBase):
    def __init__(self):
        super().__init__()
        self.driver.get(host)
    def page_input_ursename(self,username):
        self.base_input(ll.username, text=username,doc="输入用户名")
    def page_input_password(self,pwd):
        self.base_input(ll.password,text=pwd,doc="输入密码")
    def page_click_btn(self):

        self.base_click(ll.click_btn,doc="点击登录")
    # def get_title_text(self):
    #     self.driver.get
    def login(self,username,pwd):
        log.info("正在调用登录方法，username为{}，pwd为{}".format(username,pwd))
        self.page_input_ursename(username)
        self.page_input_password(pwd)
        self.page_click_btn()
        from hyht.page.homepage import HomePage
        return HomePage()



if __name__ == '__main__':
    LoginPage().login('18374977980',"443306042")
