import datetime,os,time

import allure
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.select import Select

from hyht.tools.get_log import GetLog
log=GetLog().get_logger()
from hyht.conf import BASE_PATH
class  Base:
    #初始化
    def __init__(self):
        '''解决driver问题'''
        self.driver=None

    # 截图操作
    def save_screenshot(self, doc):
        imagepath = BASE_PATH + os.sep + 'data' + os.sep +'error_image'+os.sep +'{}_{}.png'.format(doc,
                                                                                        time.strftime("%y%m%d-%H%M%S",
                                                                                        time.localtime()))

        try:
            self.driver.save_screenshot(imagepath)
            log.info("截图成功，文件路径:{}".format(imagepath))
            with allure.step(doc):
                allure.attach.file(imagepath, doc, allure.attachment_type.PNG)
        except:
            log.exception("{}-截图失败！！！".format(doc))

    # 等待元素存在
    def wait_ele_presence(self, locator, center=True, timeout=15, doc="等待元素存在"):
        """
        center：元素将在其所在滚动区的可视区域中居中对其
        timeout：最大超时时间
        doc：后续调用使用到，可以理解为标签，注释
        知识点解析：
        #scrollIntoView:
                # 如果为true，元素的顶端将和其所在滚动区的可视区域的顶端对齐。
                # 如果为false，元素的底端将和其所在滚动区的可视区域的底端对齐。
        #scrollIntoViewIfNeeded:
                #如果为true，则元素将在其所在滚动区的可视区域中居中对其。
                # 如果为false，则元素将与其所在滚动区的可视区域最近的边缘对齐。 根据可见区域最靠近元素的哪个边缘，
                # 元素的顶部将与可见区域的顶部边缘对准，或者元素的底部边缘将与可见区域的底部边缘对准。

        #log.exception('exception') #异常信息被自动添加到日志消息中
        #log.error(msg[ , *args[ , **kwargs] ] )只记录错误信息"""
        try:
            start = datetime.datetime.now()
            ele = WebDriverWait(self.driver, timeout).until(EC.presence_of_element_located(locator))#presence_of_element_located内部有解包操作
            end = datetime.datetime.now()
            log.info("{}：元素{}已存在，等待{}秒".format(doc, locator, (end - start).seconds))
            #元素将在其所在滚动区的可视区域中居中对其,元素滚动至可见位置_如果当前可见，则不会发生滚动
            self.driver.execute_script("arguments[0].scrollIntoViewIfNeeded(arguments[1]);", ele, center)
            return ele
        except :
            log.exception("{}：元素{}不存在".format(doc, locator))
            self.save_screenshot("{}-不存在".format(doc))
            raise

    # 等待元素集存在
    def wait_eles_presence(self, locator, center=True, timeout=15, doc="等待元素集存在"):
        try:
            start = datetime.datetime.now()
            eles = WebDriverWait(self.driver, timeout).until(EC.presence_of_all_elements_located(locator))#presence_of_element_located内部有解包操作
            end = datetime.datetime.now()
            log.info("{}：元素集{}已存在，等待{}秒".format(doc, locator, (end - start).seconds))
            #js控制元素将在其所在滚动区的可视区域中居中对其
            self.driver.execute_script("arguments[0].scrollIntoViewIfNeeded(arguments[1]);", eles, center)
            return eles
        except:
            log.exception("{}：元素集{}不存在".format(doc, locator))
            self.save_screenshot("{}-不存在".format(doc))
            raise

    # 等待元素可见
    def wait_ele_visible(self, locator, center=True, timeout=15, doc="等待元素集可见"):
        self.wait_ele_presence(locator, center, timeout, doc)
        try:
            start = datetime.datetime.now()
            ele = WebDriverWait(self.driver, timeout).until(EC.visibility_of_element_located(locator))
            end = datetime.datetime.now()
            log.info("{}：元素{}已可见，等待{}秒".format(doc, locator, (end - start).seconds))
            return ele
        except:
            log.exception("{}：元素{}不可见".format(doc, locator))
            self.save_screenshot("{}-不可见".format(doc))
            raise

    # 等待元素集可见
    def wait_eles_visible(self, locator, center=True, timeout=15, doc="等待元素集可见"):
        self.wait_ele_presence(locator, center, timeout, doc)
        try:
            start = datetime.datetime.now()
            eles = WebDriverWait(self.driver, timeout).until(EC.visibility_of_all_elements_located(locator))
            end = datetime.datetime.now()
            log.info("{}：元素{}已可见，等待{}秒".format(doc, locator, (end - start).seconds))
            return eles
        except:
            log.exception("{}：元素{}不可见".format(doc, locator))
            self.save_screenshot("{}-不可见".format(doc))
            raise

    # 点击操作
    def base_click(self, locator, center=True, mode=1, timeout=15, doc="点击"):
        ele = self.wait_ele_visible(locator, center, timeout, doc)
        try:
            with allure.step(doc):
                if mode == 1:
                    ele.click()
                #js点击，arguments是要将从Python 传递到要执行的 JavaScript 的内容
                if mode == 2:
                    self.driver.execute_script("arguments[0].click();", ele)
                #鼠标左键需要点击的元素
                if mode == 3:
                    ActionChains(self.driver).click(ele).perform()
            log.info("元素{}--{}成功".format(locator,doc))
        except:
            log.exception("元素{}--{}失败".format(locator,doc))
            self.save_screenshot("{}-元素失败".format(doc))
            raise

    # 输入操作
    def base_input(self, locator, text, clear=False, center=True, timeout=15, doc="输入"):
        ele = self.wait_ele_visible(locator, center, timeout, doc)
        try:
            # with allure.step(doc):
            # if clear:
            ele.clear()
            ele.send_keys(text)
            log.info("元素{},{}：{}成功".format(locator,doc, text))
        except:
            log.exception("元素{},{}：{}失败".format(locator,doc, text))
            self.save_screenshot("{}-文本失败".format(doc))
            raise

    # 获取元素的文本内容
    def get_text(self, locator, center=True, timeout=15, doc="获取元素的文本内容"):
        ele = self.wait_ele_visible(locator, center, timeout, doc)
        try:
            text = ele.text
            log.info("元素{}，{}成功，值为:{}".format(locator, doc, text))
            print(text)
            return text
        except:
            log.exception("元素{},{}失败".format(locator, doc))
            self.save_screenshot("{}-失败".format(doc))
            raise
    #获取元素集文本
    def get_text_first(self,locator, center=True, timeout=15,mode=1, doc="获取元素集文本"):

        try:
            eles = self.wait_eles_visible(locator, center, timeout, doc)
            #mode=1返回第一个文本
            if mode==1:
                ret=[one.text for one in eles][0]
                print("获取元素集返回第一个文本",ret)
                return ret
            #mode=2返回结果集所有文本
            elif mode==2:
                ret = [one.text for one in eles]
                print("获取元素集文本", ret)
                return ret
            else:
                print("mode参数错误")

            log.info("元素{}--{}成功".format(locator, doc))
        except:
            log.exception("元素{}--{}失败，检查是否为elements".format(locator, doc))
            self.save_screenshot("{}-元素失败".format(doc))
            raise


    # 下拉框处理
    def select_ele(self, locator, index=None, text=None, value=None, center=True, timeout=15, doc="下拉框"):

        ele = self.wait_ele_visible(locator, center, timeout, doc)
        try:
            with allure.step(doc):
                if index:
                    Select(ele).select_by_index(index)
                if text:
                    Select(ele).select_by_visible_text(text)
                if value:
                    Select(ele).select_by_value(value)
            log.info("元素{}，{}选择：index-{}，text-{}，value-{}成功".format(locator,doc,index, text, value))
        except:
            log.exception("元素{},{}选择失败".format(locator,doc))
            self.save_screenshot("{}-选择失败".format(doc))
            raise
        # 刷新页面

    def page_refresh(self, doc="刷新页面"):
        self.driver.refresh()
        # driver.execute_script("location.reload()")
        log.info(doc)

    # 获取元素的属性
    def get_ele_attribute(self, locator, name, center=True, timeout=15, doc="获取元素的属性"):
        '''
        :param locator:
        :param name:
        :param center:
        :param timeout:
        :param doc:
        :return: "href":"www.baidu.com",获取的属性值就是www.baidu.com
        '''
        ele = self.wait_ele_presence(locator, center, timeout, doc)
        try:
            value = ele.get_attribute(name)
            log.info("元素{},{}属性名：{}，属性值：{}成功".format(locator,doc, name, value))
            return value
        except:
            log.exception("元素{}，{}失败-属性名为：{}".format(locator,doc, name))
            self.save_screenshot("{}-失败".format(doc))
            raise

    # 获取元素的标签名
    def get_tag_name(self, locator, center=True, timeout=15, doc="获取元素的标签名"):
        ele = self.wait_ele_visible(locator, center, timeout, doc)
        try:
            tag_name = ele.tag_name
            log.info("元素{},{}为:{}".format(locator,doc,tag_name))
            return tag_name
        except:
            log.exception("{}-元素{}失败".format(locator,doc))
            self.save_screenshot("{}-失败".format(doc))
            raise



    # 等待元素出现再消失
    def wait_ele_gone(self, locator, timeout=15, poll_frequency=0.5, doc=""):
        try:
            start = datetime.datetime.now()
            WebDriverWait(self.driver, timeout, poll_frequency).until(EC.presence_of_element_located(locator))
            end = datetime.datetime.now()
            log.info("{}-元素{}已存在，等待{}秒".format(doc, locator, (end - start).seconds))
            WebDriverWait(self.driver, timeout, poll_frequency).until_not(EC.presence_of_element_located(locator))
            end = datetime.datetime.now()
            log.info("{}-元素{}已消失，等待{}秒".format(doc, locator, (end - start).seconds))
            try:
                self.save_screenshot("{}-成功".format(doc))
            finally:
                return True
        except:
            log.exception("{}-元素不消失-{}".format(doc, locator))
            try:
                self.save_screenshot("{}-元素不消失".format(doc))
            finally:
                return False

    # 断言元素文本包含指定值
    def assert_text(self, locator, expect_text, center=True, timeout=60, doc=""):
        try:
            text = self.get_text(locator, center, timeout, doc)
            result = expect_text in text
            log.info("{}-元素{}文本预期为{}，实际为{}，用例执行结果为-{}".format(doc, locator, expect_text, text, result))
            try:
                self.save_screenshot("{}-{}".format(doc, result))
            finally:
                return result
        except:
            log.exception("{}-获取文本失败，用例执行失败！！！".format(doc))
            return False
# # 查找元素
    # def get_element(self, locator, doc="查找元素"):
    #     try:
    #         log.info("{}：{}".format(doc, locator))
    #         return self.driver.find_element(*locator)
    #     except:
    #         log.exception("{}:’{}‘失败".format(doc, locator))
    #         self.save_screenshot("{}-失败".format(doc))
    #         raise
    #
    # # 查找匹配元素
    # def get_elements(self, locator, doc=""):
    #     try:
    #         log.info("{}-查找所有匹配的元素：{}".format(doc, locator))
    #         return self.driver.find_elements(*locator)
    #     except:
    #         log.exception("{}-元素集查找失败-{}".format(doc, locator))
    #         self.save_screenshot("{}-查找失败".format(doc))
    #         raise