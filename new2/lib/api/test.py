from pprint import pprint
import urllib3
urllib3.disable_warnings()#忽略警告
import requests
from new2.conf import host, user1


class Test():
    # def __init__(self,cookies):
    #     self.cookies={"X-Space-Id":cookies}
    proxies = {'http': 'http://127.0.0.1:8888', 'https': 'http://127.0.0.1:8888'}
    # def __init__(self,cookies=None):
    #     if cookies:
    #         self.header={"X-Space-Id":cookies}
    #     else:
    #         self.header =None

    def list_organiz(self, indata,cookies):
        print("正在调用列出部门接口，请求参数indata为：{}".format(indata))
        url = f'{host}/api/v4/organizations'
        params = indata
        res = requests.get(url, headers=params, cookies=cookies,proxies=self.proxies, verify=False)
        res_body = res.json()
        pprint(res_body)
        return res_body
if __name__ == '__main__':
    url = f'{host}/accounts/password/authenticate'
    data = {"user": {"email": user1['user']}, "password": user1['password'], "code": "", "locale": "zh-cn"}
    ret = requests.post(url, json=data, verify=False,
                        proxies={'http': 'http://127.0.0.1:8888', 'https': 'http://127.0.0.1:8888'})
    cookies = ret.cookies

    print(cookies)
    indata={"spaceid":cookies['X-Space-Id']}
    ret=Test().list_organiz(indata,cookies)