import requests
from new2.tools.read_before import allure_step
from new2.tools.data_process import DataProcess
from new2.tools.LOG import GetLog
import urllib3
urllib3.disable_warnings()#忽略警告
log=GetLog.get_logger()
# import urllib3
# urllib3.disable_warnings()
class Client():
    def __init__(self):
        # 确保，整个接口测试中，使用同一个requests.Session() 来管理cookie
        self.session = requests.Session()
    #fiddler 抓包使用，如果不需要可以注释，后续发送请求删除这个参数
    proxies = {'http': 'http://127.0.0.1:8888', 'https': 'http://127.0.0.1:8888'}
    def base(self,url, method, parametric_key, cookies=None,header=None, data=None, file=None,extra=None
):
        URL = DataProcess.handle_path(url)
        if header:
            header = DataProcess.handle_header(header)
        #如果请求参数不为空，就需要处理json格式或者$数据
        if data:
            data = DataProcess.handle_data(data)
            allure_step("请求数据", data)
        #如果需要上传文件就需要进行处理
        if file:
            file = DataProcess.handler_files(file)

        if parametric_key == "params":
            # res = self.session.request(method=method, url=URL, params=data, headers=header,
            #                            cookies=cookies,proxies=self.proxies,verify=False)
            res = self.session.request(method=method, url=URL, params=data, headers=header,
                                       cookies=cookies, proxies=self.proxies, verify=False)
        elif parametric_key == "data":
            res = self.session.request(method=method, url=URL, data=data, files=file, headers=header
                                       , verify=False,cookies=cookies,proxies=self.proxies)

        elif parametric_key == "json":  # {"json":data}
            res = self.session.request(method=method, url=URL, json=data, files=file, headers=header,
                                       verify=False,cookies=cookies,proxies=self.proxies)
        else:
            raise ValueError(
                '可选关键字为：get/delete/head/options/请求使用params, post/put/patch请求可使用json（application/json）/data')
        try:
            res_body=res.json()
            # log.info(f"\n最终请求地址:{URL}\n请求方法:{method}\n请求头:{header}\n请求参数:{data}\n上传文件:{file}\n响应数据:{res_body}\n提取参数:{extra}")
        except Exception as e:
            res_body=res.text
            log.error('响应结果转化为json格式出错返回text格式数据，请检查数据是否有误：{}'.format(e))

        log.info(f"\n最终请求地址:{URL}\n请求方法:{method}\n请求头:{header}\n请求参数:{data}\n上传文件:{file}\n响应数据:{res_body}")
        allure_step("响应结果", res_body)
        if extra:

            DataProcess.handle_extra(extra, res_body)
        return res_body











if __name__ == '__main__':
    # Client().base_requests(["c1", "用户名正确，密码正确", "", "/account/sLogin", "post", "params", "", {"email":"sq0777","password":"xintian","code":"","locale":"zhcn"}])
    data='{"user":{"email":"18374977980"},"password":"443306042","code":"","locale":"zh-cn"}'
    url="/accounts/password/authenticate"
    method="post"
    parametric_key="json"
    ret=Client().base(url=url,data=data,method=method,parametric_key=parametric_key)
    print(ret)
