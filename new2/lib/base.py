import time
import datetime

import allure,os

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from hyht.conf import BASE_PATH

from hyht.tools.mydriver import Get_Driver
from hyht.tools.get_log import GetLog

log=GetLog().get_logger()
class Base:
    def __init__(self):
        #获取浏览器对象
        self.driver=Get_Driver().getdriver()
    #查找元素
    def base_find(self,loc,timeout=30,poll=0.5):
        try:
            log.info("正在调用查找单个元素方法，元素为：{}".format(loc))
            element=  WebDriverWait(
                                    #传入浏览器对象
                                    self.driver,
                                    #超时时间
                                    timeout=timeout,
                                    #访问频率
                                    poll_frequency=poll).until(
                                    #
                                    lambda x:x.find_element(*loc))
            return element
        except Exception as t:
            log.error('error: "{}"元素查找失败!'.format(loc), t)
            print('error: "{}"元素查找失败!'.format(loc), t)
            raise

    def base_find_elements(self,loc,timeout=30,poll=0.5):
        try:
            log.info("正在调用查找多个元素方法，元素为：{}".format(loc))
            elements=  WebDriverWait(
                                #传入浏览器对象
                                self.driver,
                                #超时时间
                                timeout=timeout,
                                #访问频率
                                poll_frequency=poll).until(
                                #
                                lambda x:x.find_elements(*loc))

            return elements
        except Exception as t:
            log.error('error: "{}"元素集查找失败!'.format(loc), t)
            print('error: "{}"元素集查找失败!'.format(loc), t)
            raise

    # 查找元素可见
    def wait_ele_visible(self, loc, timeout=15):
        self.base_find(loc, timeout)
        try:
            start = datetime.datetime.now()
            ele = WebDriverWait(self.driver, timeout).until(EC.visibility_of_element_located(loc))
            end = datetime.datetime.now()
            log.info("元素{}已可见，等待{}秒".format(loc, (end - start).seconds))
            return ele
        except:
            log.exception("元素不可见-{}".format(loc))
            # self.save_screenshot("{}-元素不可见".format(doc))
            raise
    #查找元素集可见
    def wait_eles_visible(self, loc, timeout=15):
        self.base_find(loc, timeout)
        try:
            start = datetime.datetime.now()
            eles = WebDriverWait(self.driver, timeout).until(EC.visibility_of_element_located(loc))
            end = datetime.datetime.now()
            log.info("元素{}已可见，等待{}秒".format(loc, (end - start).seconds))
            return eles
        except:
            log.exception("元素不可见-{}".format(loc))
            # self.save_screenshot("{}-元素不可见".format(doc))
            raise

    # 点击元素方法
    def base_click(self,loc):
        try:
            log.info("正在调用点击元素方法，元素为：{}".format(loc))
            self.wait_ele_visible(loc).click()
        except Exception as t:
            log.exception('"{}"点击元素失败!'.format(loc), t)
            print('error: "{}"点击元素失败!'.format(loc), t)
            raise
    #输入内容方法
    def base_input(self,loc,value):
        try:
            log.info("正在调用输入内容方法，元素为：{}，输入内容为：{}".format(loc,value))
            el=self.wait_ele_visible(loc)
            el.clear()
            el.send_keys(value)
        except Exception as t:
            log.error('"{}"输入内容失败!'.format(loc), t)
            print('error: "{}"输入内容失败!'.format(loc), t)
            raise
    #获取文本方法
    def base_get_text(self,loc):
        try:
            log.info("正在调用获取元素文本方法，元素为：{}".format(loc))
            return self.wait_ele_visible(loc).text
        except Exception as t:
            log.error('"{}"文本获取失败!'.format(loc), t)
            print('error: "{}"文本获取失败!'.format(loc), t)
            raise

    # 截图
    def base_get_img(self):
        imagepath=BASE_PATH+ os.sep+'data'+ os.sep+'image'+'err.png'
        # 1 调用截图方法
        log.error("断言出错，正在调用截图!")
        self.driver.get_screenshot_as_file(imagepath)
        # 2 调用图片写入报告方法
        log.error("正在将截图写入报告!")
        self.__base_write_img()

    # 将图片写入报告,私有所以用__base
    def __base_write_img(self):
        imagepath = BASE_PATH + os.sep + 'data' + os.sep + 'image' + 'err.png'
        # 1 获取图片文件流
        with open(imagepath, "rb") as f:
            # 2 调用allure.attach附加方法
            allure.attach("错误原因：", f.read(), allure.attachment_type.PNG)  # 错误原因，图片流，图片类型

    '''以下为web项目专属方法'''

    # 根据文本内容，点击指定元素
    def web_base_click(self,click_text):
        log.info("正在调用 根据文本内容，点击指定元素方法，文本为：{}".format(click_text))
        loc = By.XPATH, "//*[text()='{}']".format(click_text)
        self.base_click(loc)

    # 根据文本内容，下拉选择指定文本方法
    def web_base_click_element(self, placeholder_text, click_text):
        log.info("正在调用 根据文本内容，下拉选择指定文本方法，父选框为：{}，选择的文本为：{}".format(placeholder_text,click_text))
        # 点击父选框
        loc = By.CSS_SELECTOR, '[placeholder="{}"]'.format(placeholder_text)
        self.base_click(loc)
        # 暂停
        time.sleep(1)
        # 点击包含的文本的元素
        loc = By.XPATH, "//*[text()='{}']".format(click_text)
        self.base_click(loc)

    # 判断页面是否包含指定的元素
    def web_base_is_exist(self, text):
        log.info("正在调用 web元素是否存在方法，文本为：{}".format(text))
        loc = By.XPATH, "//*[text()='{}']".format(text)
        try:
            # 1.找元素
            self.base_find(loc, timeout=3)
            print("找到{}元素啦".format(loc))
            return True

        # 输出找到信息
        except:
            print("未找到{}元素".format(loc))
            return False
