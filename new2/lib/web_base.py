from time import sleep
import win32com.client
import allure,datetime,time,os
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from hyht.lib.base2 import Base
from hyht.tools.mydriver import Get_Driver
from selenium.webdriver.common.action_chains import ActionChains
from hyht.conf import BASE_PATH
from hyht.tools.get_log import GetLog
log=GetLog().get_logger()
class WebBase(Base):
    def __init__(self):
        super().__init__()
        self.driver=Get_Driver().getdriver()

    # 鼠标悬停
    def hover(self, locator, center=True, timeout=15, doc="鼠标悬停"):
        ele = self.wait_ele_presence(locator, center, timeout, doc)
        try:
            with allure.step(doc):
                ActionChains(self.driver).move_to_element(ele).perform()
            log.info("元素{}-{}成功:".format(locator,doc))
        except:
            log.exception("元素{}-{}失败".format(locator,doc))
            self.save_screenshot("{}-失败".format(doc))
            raise
            # iframe处理

    def switch_iframe(self, iframe_reference, timeout=60, doc="进入iframe"):
        """"""
        try:
            start = datetime.datetime.now()
            WebDriverWait(self.driver, timeout).until(EC.frame_to_be_available_and_switch_to_it(iframe_reference))
            end = datetime.datetime.now()
            log.info("{}成功-等待{}秒".format(doc, (end - start).seconds))
        except:
            log.exception("{}-失败-{}".format(doc, iframe_reference))
            self.save_screenshot("{}-失败".format(doc))
            raise
    # 退回上层表单
    def switch_parent_iframe(self, doc="退回上层表单"):
        self.driver.switch_to.parent_frame()
        log.info(doc)
    # 退回初始表单
    def switch_default_iframe(self, doc="退回初始表单"):
        self.driver.switch_to.default_content()
        log.info(doc)

    def is_alert(self,timeout=10,doc="alert对话框"):
        """
        assert alert if exsit
        :return: alert obj
        """
        #正常返回对象，异常返回None，为后续判断提供值
        try:
            re=WebDriverWait(self.driver,timeout).until(EC.alert_is_present())
            log.info("调用查找{}方法".format(doc))
            return re

        except  Exception :
            log.exception("{}-不存在".format(doc))
            print("error:no found alert")
            return None
    # 获取异常alert文本
    def get_alert_txt(self,timeout=10):
        time.sleep(1)
        WebDriverWait(self.driver,timeout).until(EC.alert_is_present())
        self.driver.switch_to.alert.accept()
        #如果alert存在，返回txt文本，不存在返回空
        if self.is_alert()!=None:
            ret=self.is_alert().text# 得到对话框文本内容
            self.is_alert().accept()# 点击确定按钮
            return   ret
        else:
            return None
    #关闭alert弹框
    def close_alert(self):
        self.is_alert().accept()
    # 上传文件
    def upload_file_input(self, locator, filename,doc="上传文件"):
        log.info("{}：文件名为：{}，元素为{}".format(doc, filename,locator))
        files = BASE_PATH + os.sep + 'data' + os.sep + filename
        try:

            self.base_input(locator, files)
            sleep(5)
        except Exception:
            log.exception("{}：{}失败！".format(doc,files))
            self.save_screenshot(doc)
            raise
    # 非input上传文件
    def upload_file_uninput(self, locator, filename,doc="非input上传文件"):
        log.info("{}：文件名为：{}，元素为：{}".format(doc,filename,locator))
        files = BASE_PATH + os.sep + 'data' + os.sep + filename
        try:
            # 调用鼠标事件点击上传
            ActionChains(self.driver).click(self.wait_ele_presence(locator)).perform()
            sleep(1)
            shell = win32com.client.Dispatch("WScript.Shell")
            shell.Sendkeys(files + "\r\n")
            sleep(5)
        except  Exception:
            log.exception("{}：{}失败".format(doc,filename))
            self.save_screenshot(doc)
            raise
    def enter(self,doc="回车确认"):
        log.info(doc)
        ActionChains(self.driver).send_keys(Keys.ENTER).perform()


        # 异常弹框流程

    # def abnormal_bullet_frame(self):
    #     log.info("正在调用异常弹框流程方法")
    #     sleep(1)
    #     self.base_click(page.tea_sure_add_titles_btn)
    #     sleep(1)
    #     # 提示信息
    #     msg = self.locator(page.tea_course_info)
    #     # 弹框提示确认
    #     self.base_click(page.tea_course_info_btn)
    #     print(msg)
    #     return msg

        # self.driver.switch_to.alert.accept()#点击ok
        # self.driver.switch_to.alert.text#得到对话框文本内容
        # self.driver.switch_to.alert.dismiss()#点击cancel
        # self.driver.switch_to.alert.send_keys()#输入内容