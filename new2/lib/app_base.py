from selenium.webdriver.common.by import By
from hyht.conf import BASE_PATH
from hyht.tools.mydriver import Get_Driver
import allure,os,datetime,time
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from hyht.tools.get_log import GetLog
log = GetLog().get_logger()

class AppBase:
    def __init__(self):
        self.driver=Get_Driver().get_app_driver()
    # 截图操作
    def save_screenshot(self, doc):
        imagepath = BASE_PATH + os.sep + 'data' + os.sep + 'error_image' + os.sep + '{}_{}.png'.format(doc,
                                                                                                       time.strftime(
                                                                                                           "%y%m%d-%H%M%S",
                                                                                                           time.localtime()))

        try:
            self.driver.save_screenshot(imagepath)
            log.info("截图成功，文件路径:{}".format(imagepath))
            with allure.step(doc):
                allure.attach.file(imagepath, doc, allure.attachment_type.PNG)
        except:
            log.exception("{}-截图失败！！！".format(doc))

    # 等待元素存在
    def wait_ele_presence(self, locator,timeout=15, doc="等待元素存在"):
        #log.exception('exception') #异常信息被自动添加到日志消息中
        #log.error(msg[ , *args[ , **kwargs] ] )只记录错误信息"""
        try:
            start = datetime.datetime.now()
            ele = WebDriverWait(self.driver, timeout).until(
                EC.presence_of_element_located(locator))  # presence_of_element_located内部有解包操作
            end = datetime.datetime.now()
            log.info("{}：元素{}已存在，等待{}秒".format(doc, locator, (end - start).seconds))
            return ele
        except:
            log.exception("{}：元素{}不存在".format(doc, locator))
            self.save_screenshot("{}-不存在".format(doc))
            raise

    # 等待元素集存在
    def wait_eles_presence(self, locator, timeout=15, doc="等待元素集存在"):
        try:
            start = datetime.datetime.now()
            eles = WebDriverWait(self.driver, timeout).until(
                EC.presence_of_all_elements_located(locator))  # presence_of_element_located内部有解包操作
            end = datetime.datetime.now()
            log.info("{}：元素集{}已存在，等待{}秒".format(doc, locator, (end - start).seconds))
            return eles
        except:
            log.exception("{}：元素集{}不存在".format(doc, locator))
            self.save_screenshot("{}-不存在".format(doc))
            raise

    # 等待元素可见
    def wait_ele_visible(self, locator,  timeout=15, doc="等待元素集可见"):
        self.wait_ele_presence(locator, timeout, doc)
        try:
            start = datetime.datetime.now()
            ele = WebDriverWait(self.driver, timeout).until(EC.visibility_of_element_located(locator))
            end = datetime.datetime.now()
            log.info("{}：元素{}已可见，等待{}秒".format(doc, locator, (end - start).seconds))
            return ele
        except:
            log.exception("{}：元素{}不可见".format(doc, locator))
            self.save_screenshot("{}-不可见".format(doc))
            raise

    # 等待元素集可见
    def wait_eles_visible(self, locator,  timeout=15, doc="等待元素集可见"):
        self.wait_ele_presence(locator, timeout, doc)
        try:
            start = datetime.datetime.now()
            eles = WebDriverWait(self.driver, timeout).until(EC.visibility_of_all_elements_located(locator))
            end = datetime.datetime.now()
            log.info("{}：元素{}已可见，等待{}秒".format(doc, locator, (end - start).seconds))
            return eles
        except:
            log.exception("{}：元素{}不可见".format(doc, locator))
            self.save_screenshot("{}-不可见".format(doc))
            raise

    # 点击操作
    def base_click(self, locator,  mode=1, timeout=15, doc="点击"):
        ele = self.wait_ele_visible(locator,timeout, doc)
        try:
            ele.click()
            log.info("元素{}--{}成功".format(locator, doc))
        except:
            log.exception("元素{}--{}失败".format(locator, doc))
            self.save_screenshot("{}-元素失败".format(doc))
            raise

    # 输入操作
    def base_input(self, locator, text, clear=False,  timeout=15, doc="输入"):
        ele = self.wait_ele_visible(locator,  timeout, doc)
        try:

            ele.clear()
            ele.send_keys(text)
            log.info("元素{},{}：{}成功".format(locator, doc, text))
        except:
            log.exception("元素{},{}：{}失败".format(locator, doc, text))
            self.save_screenshot("{}-文本失败".format(doc))
            raise

    # 获取元素的文本内容
    def get_text(self, locator,  timeout=15, doc="获取元素的文本内容"):
        ele = self.wait_ele_visible(locator, timeout, doc)
        try:
            text = ele.text
            log.info("元素{}，{}成功，值为:{}".format(locator, doc, text))
            print(text)
            return text
        except:
            log.exception("元素{},{}失败".format(locator, doc))
            self.save_screenshot("{}-失败".format(doc))
            raise

# 获取元素集文本
def get_text_first(self, locator,  timeout=15, mode=1, doc="获取元素集文本"):

    try:
        eles = self.wait_eles_visible(locator,timeout, doc)
        # mode=1返回第一个文本
        if mode == 1:
            ret = [one.text for one in eles][0]
            print("获取元素集返回第一个文本", ret)
            return ret
        # mode=2返回结果集所有文本
        elif mode == 2:
            ret = [one.text for one in eles]
            print("获取元素集文本", ret)
            return ret
        else:
            print("mode参数错误")

        log.info("元素{}--{}成功".format(locator, doc))
    except:
        log.exception("元素{}--{}失败，检查是否为elements".format(locator, doc))
        self.save_screenshot("{}-元素失败".format(doc))
        raise

    # #1.查找页面是否存在指定元素
    # def app_base_is_exist(self,locator):
    #     try:
    #         #1.调用查找方法
    #         self.base_click(locator,timeout=3)
    #         #2.输出信息
    #         print("找到：{}元素啦！".format(locator))
    #         #3.返回true
    #         return True
    #     except:
    #         #1.输出信息
    #         print("未找到：{}元素！".format(locator))
    #         #2.返回false
    #         return False
    # #从右向左滑动屏幕
    # def app_base_right_left(self,loc_area,click_text,doc):
    #     #1.查找区域元素
    #     el=self.base_click(loc_area)
    #     # 2.获取区域元素的位置y坐标点
    #     y=el.location.get('y')
    #     # 3.获取区域元素宽高
    #     width=el.size.get('width')
    #     height=el.size.get('height')
    #     # 4.计算start_x，start_y,end_x,end_y
    #     start_x=width*0.8#屏幕宽度的百分之八十
    #     start_y=y+height*0.5
    #     end_x=width *0.2#结束点在百分20的位置
    #     end_y=start_y
    #     loc=By.XPATH,'//*[contains(@text,"{}")]'.format(click_text)
    #     # 5.循环操作
    #     while True:
    #         #获取当前屏幕的页面结构
    #         page_source=self.driver.page_source
    #         el=self.base_click(loc,doc='')
    #         break
    #     self.driver.swipe(start_x,start_y,end_x,end_y,duration=2000)
    #     if page_source ==self.driver.page_source
    #         print("滑动到屏幕最后一页")
